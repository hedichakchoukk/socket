package server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class ServerThread extends  Thread{
    private Socket socket;
    private ArrayList<ServerThread> threadArrayList;
    private PrintWriter output;

    public  ServerThread(Socket socket,ArrayList<ServerThread>threads)
    {
        this.socket=socket;
        this.threadArrayList=threads;
    }
    @Override
    public void run(){
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader((socket.getInputStream())));
            output = new PrintWriter(socket.getOutputStream(),true);

            while (true)
            {
                String outPutString = input.readLine();
                if(outPutString.equals("exit")){
                    break;
                }
                printToAllClients(outPutString);
                System.out.println("Server received"+outPutString);
            }
        } catch (Exception e) {
            System.out.println("error in thread"+e.getStackTrace());
        }
    }
    private void printToAllClients(String outputString){
        for(ServerThread serverThread:threadArrayList)
        {
            serverThread.output.println(outputString);
        }
    }

}
