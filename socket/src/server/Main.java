package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Main {

    public static  void  main(String[] args)
    {
        ArrayList<ServerThread> threadArrayList = new ArrayList<>();
        try (ServerSocket serverSocket = new ServerSocket(5000)){
            while (true){
                Socket socket = serverSocket.accept();
                ServerThread serverThread = new ServerThread(socket,threadArrayList);
                threadArrayList.add(serverThread);
                serverThread.start();
            }
        } catch (Exception e) {
            System.out.println("error in main"+e.getStackTrace());
        }
    }
}
